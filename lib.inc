%define SYSCALL_SYS_READ 0
%define SYSCALL_SYS_WRITE 1
%define SYSCALL_SYS_EXIT 60

%define STDIN_FILENO 0
%define STDOUT_FILENO 1

%define MAX_LENGTH_OF_ULONG_IN_STR 21

section .text
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, SYSCALL_SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rcx, -1
.loop:
    inc rcx
    mov dl, [rdi+rcx]
    test rdx, rdx
    jnz .loop ; читаем пока не встретили 0 (\0)
    mov rax, rcx
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rdi, STDOUT_FILENO
    mov rax, SYSCALL_SYS_WRITE
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp ; char *buf
    mov rdi, 1 ; size_t count
    mov rax, STDOUT_FILENO
    mov rdx, SYSCALL_SYS_WRITE
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`
    call print_char
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    test rdi, rdi
    jns print_uint ; если число положительно, то просто выводим его (Jump if Not Signed)
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    ; print_uint

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi ; число в rax
    mov r8, 10 ; делитель
    enter MAX_LENGTH_OF_ULONG_IN_STR, 0 ; char[21] столько занимает unsigned long в виде строки
    mov r9, rbp ; char*
    dec r9 ; r9 указывает на push rbp, смещаемся вверх по стеку
    mov [rbp], byte 0 ; \0 в самый последний элемент стека
.loop:
    dec r9 ; перемещаемся по стеку (адрес куда писать символ)
    xor rdx, rdx
    div r8
    add rdx, '0' ; перевод в ASCII
    mov byte [r9], dl
    test rax, rax
    jnz .loop
    mov rdi, r9
    call print_string
    leave
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    mov rax, 1 ; предполагаем, что строки равны и пытаемся опровергнуть
.loop:
    mov r8b, byte[rdi+rcx]
    mov r9b, byte[rsi+rcx]
    test r8b, r9b
    jz .ret ; предварительный return. Либо они оба '\0', либо отличаются
    inc rcx
    cmp r8b, r9b
    je .loop ; продолжаем если равны
    xor rax, rax
.ret:
    cmp r8b, r9b
    je .ret2
    xor rax, rax
.ret2:
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rdi, rdi
    push rdi
    mov rax, SYSCALL_SYS_READ
    mov rdi, STDIN_FILENO
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    inc rax
    test rax, rax
    jnz .ret
    xor rdi, rdi
.ret:
    mov rax, rdi
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12 ; т.к. часто вызывается read_char, то используем callee-saved регистры
    push r13
    push r14
    mov r12, rdi ; char *buf
    mov r13, rsi ; length
    dec r13 ; length - \0
    xor r14, r14 ; counter
.skip_loop: ; покинем только после встречи первого не пробельного символа
    call read_char
    cmp rax, ' '
    je .skip_loop
    cmp rax, `\t`
    je .skip_loop
    cmp rax, `\n`
    je .skip_loop
.loop: ; покинем при встречи пробельного символа и терминанта
    mov [r12+r14], rax
    test rax, rax
    jz .ret ; терминант
    cmp rax, ' '
    je .ret ; пробельный
    cmp rax, `\t`
    je .ret ; пробельный
    cmp rax, `\n`
    je .ret ; пробельный
    cmp r13, r14
    je .ret_err ; +`\0` -> не помещается в буфер
    inc r14
    call read_char
    jmp .loop
.ret_err:
    xor r12, r12
    xor r14, r14
.ret:
    mov rax, r12 ; char *buf
    mov rdx, r14 ; length
    pop r14
    pop r13
    pop r12
    ret
 
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx ; counter
    mov r8, 10
    xor r9, r9
.loop:
    mov r9b, byte[rdi+rcx]
    test r9, r9
    jz .ret ; терминатор
    cmp r9, '0'
    jb .ret ; < '0' (возвращаем то, что распарсили)
    cmp r9, '9'
    ja .ret ; > '9' (возвращаем то, что распарсили)
    sub r9, '0' ; из ASCII в цифровое значение
    xor rdx, rdx
    mul r8 ; rax *= 10
    jc .ret_err ; превышает unsigned long
    add rax, r9 ; rax += r9
    inc rcx ; next symbol
    jmp .loop
.ret_err:
    xor rcx, rcx ; rdx = 0 если число прочитать не удалось
.ret:
    mov rdx, rcx
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    push r12
    mov r12b, byte[rdi]
    cmp r12b, '-'
    jne .skip ; если в начале нет '-', то parse_uint, иначе запоминаем '-'
    mov al, byte[rdi+1]
    cmp al, '0'
    jb .ret_err ; после '-' ожидаем число, а не чьл-то другое
    cmp al, '9'
    ja .ret_err ; после '-' ожидаем число, а не чьл-то другое
    inc rdi
.skip:
    call parse_uint
    cmp r12b, '-'
    jne .ret ; если запомнили '-' то в дополнительный код
    inc rdx
    neg rax
    jmp .ret
.ret_err:
    xor rax, rax
    xor rdx, rdx
.ret:
    pop r12
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx ; counter
.loop:
    test rdx, rdx
    jz .ret_err ; не влазит в буфер
    mov r8b, [rdi+rcx] ; из строки
    mov [rsi+rcx], r8b ; в буфер
    test r8b, r8b
    jz .ret ; терминатор
    inc rcx
    dec rdx
    jmp .loop
.ret_err:
    xor rcx, rcx
.ret:
    mov rax, rcx
    ret
